#include "luat_base.h"
#include "luat_malloc.h"
#include "luat_fs.h"
#include "stdio.h"
#include "luat_msgbus.h"
#include "luat_timer.h"

#define LUAT_LOG_TAG "luat.main"
#include "luat_log.h"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "evm.h"
#include <time.h>
#include "wrap_luat.h"
#include <rtthread.h>

#define SAMPLE_UART_NAME       "uart0"    /* 串口设备名称 */
static rt_device_t serial;                /* 串口设备句柄 */    
/* 查找串口设备 */

/*****************REPL*******************/
// 定义REPL接口函数evm_repl_tty_read，从tty终端获取字符
#ifdef EVM_LANG_ENABLE_REPL
char evm_repl_tty_read(evm_t * e)
{
    EVM_UNUSED(e);
    char ch;
    while (rt_device_read(serial, -1, &ch, 1) != 1){

    }
    return ch;
}
#endif

int luat_main (void) {
//   if (boot_mode == 0) {
//     return 0; // just nop
//   }
//   LLOGI("LuatOS@%s %s, Build: " __DATE__ " " __TIME__, luat_os_bsp(), LUAT_VERSION);
//   #if LUAT_VERSION_BETA
//   LLOGD("This is a beta version, for testing");
//   #endif
  // 1. 初始化文件系统
  luat_fs_init();

  serial = rt_device_find(SAMPLE_UART_NAME);

  // // 2. 是否需要升级?
  // check_update();

  // // 3. 是否需要回滚呢?
  // check_rollback();

//   // 4. init Lua State
//   int status, result;
//   L = lua_newstate(luat_heap_alloc, NULL);
//   if (L == NULL) {
//     l_message("LUAVM", "cannot create state: not enough memory\n");
//     goto _exit;
//   }
//   if (L) lua_atpanic(L, &panic);
//   //print_list_mem("after lua_newstate");
//   lua_pushcfunction(L, &pmain);  /* to call 'pmain' in protected mode */
//   //lua_pushinteger(L, argc);  /* 1st argument */
//   //lua_pushlightuserdata(L, argv); /* 2nd argument */
//   status = lua_pcall(L, 0, 1, 0);  /* do the call */
//   result = lua_toboolean(L, -1);  /* get result */
//   report(L, status);
//   //lua_close(L);
// _exit:
//   LLOGE("Lua VM exit!! reboot in 30s");
//   // 既然是异常退出,那肯定出错了!!!
//   // 如果升级过, 那么就写入标志文件
//   {
//     if (luat_fs_fexist(UPDATE_MARK)) {
//       FILE* fd = luat_fs_fopen("/rollback_mark", "wb");
//       if (fd) {
//         luat_fs_fclose(fd);
//       }
//     }
//     else {
//       // 没升级过, 那就是线刷, 不存在回滚
//     }
//   }
//   // 等30秒,就重启吧
//   luat_timer_mdelay(30*1000);
//   luat_os_reboot(result);
  // 往下是肯定不会被执行的

  wrap_luat_main();
  return 0;
}

//#include "vsprintf.h"
// #include "printf.h"
// __attribute__((weak)) int l_sprintf(char *buf, size_t size, const char *fmt, ...) {
//     int32_t n;
//     va_list args;

//     va_start(args, fmt);
//     //n = custom_vsprintf(buf, /*size,*/ fmt, args);
//     n = vsnprintf_(buf, size, fmt, args);
//     va_end(args);

//     return n;
// }
